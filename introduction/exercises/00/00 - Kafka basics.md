# Exercise 0: Learn Kafka basics 

1. From the [Quick Start document](https://kafka.apache.org/quickstart) in the official documentation of Apache Kafka:
   - Set up Kafka (steps 1 and 2).
   - Go through step 3 (create a topic).
   - Go through step 4 (write to the topic).
   - Go through step 5 (read from the topic).
   - Send a few messages and check the order in which the messages arrive in the consumer.
   
2. Create another topic, but now one with 10 partitions
   - Start a new producer that writes to the new topic and send a few messages
   - Start a new consumer that reads from the new topic.
   - Check the read order of the messages. Could you why the order is different from the first topic?
   
3. Stop the producer and consumer clients with Ctrl-C


